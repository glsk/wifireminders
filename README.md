# Wi-Fi Reminders

Utility which shows custom notifications when connected to specified Wi-Fi networks or mobile cell.

Due to Android 8 (Oreo) battery saving restrictions starting from version 1.3.0 this app will only poll connected networks every 15 minutes. That means if you stay connected for shorter period, the reminder may not appear.

You can also specify mobile network CellID to trigger reminder if no Wifi connection is available. This method may be unreliable if your device is constantly switching between cells it connects to.

With version 1.2.6 and below running on Android 7 (Nougat) and below you could set reminders for any connected or just available (with polling turned on) wireless network.

## Screenshots

[![](app/src/main/play/en-US/phoneScreenshots/.thumb/t01.jpg)](app/src/main/play/en-US/phoneScreenshots/01.png) [![](app/src/main/play/en-US/phoneScreenshots/.thumb/t02.jpg)](app/src/main/play/en-US/phoneScreenshots/02.png) [![](app/src/main/play/en-US/phoneScreenshots/.thumb/t03.jpg)](app/src/main/play/en-US/phoneScreenshots/03.png) [![](app/src/main/play/en-US/phoneScreenshots/.thumb/t04.jpg)](app/src/main/play/en-US/phoneScreenshots/04.png) [![](app/src/main/play/en-US/phoneScreenshots/.thumb/t05.jpg)](app/src/main/play/en-US/phoneScreenshots/05.png)

## Download

[![t10]][10]

[t10]: https://glsk.net/wp-content/uploads/2013/08/get_it_on_f-droid_45.png
[10]: https://f-droid.org/repository/browse/?fdid=ru.glesik.wifireminders

## Donations

[https://glsk.net/donate/](https://glsk.net/donate/)

## License

Copyright (c) 2016 Alexander Inglessi (https://glsk.net).

Code licensed under [GNU GPL](http://www.gnu.org/licenses/gpl.html) version 3 or any later version. Launcher and notification artwork licensed under [CC BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/3.0/) 3 or any later version.
