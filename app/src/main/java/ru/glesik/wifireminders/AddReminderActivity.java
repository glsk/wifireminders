/*
 * This file is part of Wi-Fi Reminders.
 *
 * Wi-Fi Reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wi-Fi Reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wi-Fi Reminders.  If not, see <http://www.gnu.org/licenses/>.
 */

package ru.glesik.wifireminders;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import java.util.HashSet;
import java.util.List;

public class AddReminderActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_reminder);

        // Save reminder FAB.
        FloatingActionButton fab = findViewById(R.id.floatingActionButtonDone);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Check if title is empty.
                EditText editRuleTitle = findViewById(R.id.editTitle);
                String title = editRuleTitle.getText().toString();
                if (TextUtils.isEmpty(title)) {
                    editRuleTitle.setError(getText(R.string.error_no_title));
                    return;
                }
                // Save data to SharedPreferences.
                AutoCompleteTextView textViewSsid = findViewById(R.id.autoCompleteTextViewSSID);
                String selectedSSID = textViewSsid.getText().toString();
                AutoCompleteTextView textViewCell = findViewById(R.id.autoCompleteTextViewCell);
                String selectedCell = textViewCell.getText().toString();
                SharedPreferences sharedPrefRules = getSharedPreferences("rules",
                        MODE_PRIVATE);
                // Get number of rules.
                int rulesCount = sharedPrefRules.getInt("RulesCount", 0);
                // Save new rule to SharedPreferences.
                SharedPreferences.Editor editor = sharedPrefRules.edit();
                EditText editReminderText = findViewById(R.id.editReminderText);
                editor.putString("Title" + Integer.toString(rulesCount + 1),
                        editRuleTitle.getText().toString());
                editor.putString("Text" + Integer.toString(rulesCount + 1),
                        editReminderText.getText().toString());
                editor.putString("SSID" + Integer.toString(rulesCount + 1),
                        selectedSSID);
                editor.putString("Cell" + Integer.toString(rulesCount + 1),
                        selectedCell);
                editor.putBoolean("Enabled" + Integer.toString(rulesCount + 1),
                        true);
                editor.putInt("RulesCount", rulesCount + 1);
                editor.commit();
                AddReminderActivity.this.finish();
            }
        });

        // Get stored Wifi networks.
        ArrayAdapter<String> adapterSsid = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line);
        final AutoCompleteTextView textViewSsid = findViewById(R.id.autoCompleteTextViewSSID);
        textViewSsid.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                textViewSsid.showDropDown();
            }
        });
        textViewSsid.setAdapter(adapterSsid);
        textViewSsid.setThreshold(1);

        WifiManager mainWifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        assert mainWifi != null;
        if (mainWifi.isWifiEnabled()) {
            mainWifi.startScan();
            // Get list of stored networks.
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                List<WifiConfiguration> wifiList = mainWifi.getConfiguredNetworks();
                for (WifiConfiguration result : wifiList) {
                    // Remove quotes.
                    adapterSsid.add(result.SSID.replaceAll(
                            "^\"|\"$", ""));
                }
                adapterSsid.notifyDataSetChanged();
            }
            // Select current SSID in the spinner.
            String cSsid = mainWifi.getConnectionInfo().getSSID();
            if (cSsid != null) {
                cSsid = cSsid.replaceAll("^\"|\"$", "");
                if (adapterSsid.getCount() == 0) {  // getConfiguredNetworks returns empty list on Android 10+.
                    adapterSsid.add(cSsid);
                    adapterSsid.notifyDataSetChanged();
                }
                textViewSsid.setText(cSsid);
            }
        } else {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.error_wifi_off_title)
                    .setMessage(R.string.error_wifi_off_text)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));
                            finish();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
        // Get connected CellID.
        ArrayAdapter<String> adapterCell = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line);
        final AutoCompleteTextView textViewCell = findViewById(R.id.autoCompleteTextViewCell);
        textViewCell.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                textViewCell.showDropDown();
            }
        });
        textViewCell.setAdapter(adapterCell);
        final TelephonyManager telephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        assert telephony != null;
        if (telephony.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                List<CellInfo> cellInfoList = telephony.getAllCellInfo();
                if (cellInfoList != null) {
                    HashSet<String> cellList = new HashSet<>();
                    for (CellInfo ci : cellInfoList) {
                        if (ci.isRegistered()) {
                            String cellId = "";
                            if (ci instanceof CellInfoGsm) {
                                CellIdentityGsm identityGsm = ((CellInfoGsm) ci).getCellIdentity();
                                cellId = Integer.toString(identityGsm.getCid());
                            } else if (ci instanceof CellInfoCdma) {
                                CellIdentityCdma identityCdma = ((CellInfoCdma) ci).getCellIdentity();
                                cellId = Integer.toString(identityCdma.getBasestationId());
                            } else if (ci instanceof CellInfoWcdma) {
                                CellIdentityWcdma identityWcdma = ((CellInfoWcdma) ci).getCellIdentity();
                                cellId = Integer.toString(identityWcdma.getCid());
                            } else if (ci instanceof CellInfoLte) {
                                CellIdentityLte identityLte = ((CellInfoLte) ci).getCellIdentity();
                                cellId = Integer.toString(identityLte.getCi());
                            }
                            cellList.add(cellId);
                        }
                    }
                    adapterCell.addAll(cellList);
                }
            }
        }
        adapterCell.notifyDataSetChanged();
    }

}
