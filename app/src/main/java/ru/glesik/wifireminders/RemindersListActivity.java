/*
 * This file is part of Wi-Fi Reminders.
 *
 * Wi-Fi Reminders is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Wi-Fi Reminders is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Wi-Fi Reminders.  If not, see <http://www.gnu.org/licenses/>.
 */

package ru.glesik.wifireminders;

import android.Manifest;
import android.app.AlertDialog;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class RemindersListActivity extends AppCompatActivity {

    private static final int PERMISSIONS_REQUEST_CODE_ACCESS_LOCATION = 1;
    public static final int WIFI_POLLING_JOB = 1;
    public static final String REFRESH_LIST = "REFRESH_LIST";
    private static final int INTERVAL = 60000;  // 1 minute, although Android 8 sets job interval to 15 min.

    private JobScheduler mJobScheduler;
    private BroadcastReceiver rulesUpdateReceiver;
    ArrayList <RemindersListDataModel> dataModels;
    ListView listView;
    private RemindersListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminders_list);

        if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION},
                        PERMISSIONS_REQUEST_CODE_ACCESS_LOCATION);
            } else {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST_CODE_ACCESS_LOCATION);
            }
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(REFRESH_LIST);
        rulesUpdateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("rulesUpdateReceiver", "received");
                refreshList(true);
            }
        };
        registerReceiver(rulesUpdateReceiver, filter);

        // Add reminder FAB.
        FloatingActionButton fab = findViewById(R.id.floatingActionButtonAdd);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ari = new Intent(RemindersListActivity.this, AddReminderActivity.class);
                startActivity(ari);
            }
        });

        // Create adapter for remindersListView.
        listView = findViewById(R.id.remindersListView);

        dataModels = new ArrayList<>();

        adapter = new RemindersListAdapter(dataModels, getApplicationContext());

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView <?> parent, View view, final int position, long id) {
                final SharedPreferences sharedPrefRulesE = getSharedPreferences(
                        "rules", MODE_PRIVATE);
                // Show dialog with selected item's reminder text to edit.
                AlertDialog.Builder alert = new AlertDialog.Builder(RemindersListActivity.this);
                final EditText input = new EditText(RemindersListActivity.this);
                input.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                        | InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_CLASS_TEXT
                        | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT);
                input.setSingleLine(false);
                input.setHint(R.string.hint_reminder);
                input.setFocusableInTouchMode(true);
                input.setText(sharedPrefRulesE.getString(
                        "Text" + (position + 1), "error"));
                alert.setView(input);
                alert.setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                // OK pressed: store new value and refresh list.
                                String value = input.getText().toString();
                                SharedPreferences.Editor editor = sharedPrefRulesE
                                        .edit();
                                editor.putString("Text" + (position + 1), value);
                                if (!value.equals("")) {
                                    editor.putBoolean("Enabled" + (position + 1), true);
                                }
                                editor.commit();
                                refreshList(true);
                            }
                        });
                alert.setNegativeButton(android.R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                // Cancel pressed.
                            }
                        });
                final AlertDialog dialog = alert.create();
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialog.show();
                input.requestFocus();
            }
        });
        registerForContextMenu(listView);
        // Read default settings in case user hasn't set any.
        PreferenceManager.setDefaultValues(this, R.xml.settings, false);
        mJobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshList(true);
    }

    @Override
    protected void onDestroy() {
        if (rulesUpdateReceiver != null) {
            unregisterReceiver(rulesUpdateReceiver);
            rulesUpdateReceiver = null;
        }
        super.onDestroy();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        // Inflate the menu.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.reminders_list_context, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
                .getMenuInfo();
        switch (item.getItemId()) {
            case R.id.action_delete:
                // Delete selected.
                SharedPreferences sharedPrefRulesD = getSharedPreferences("rules",
                        MODE_PRIVATE);
                int rulesCount = sharedPrefRulesD.getInt("RulesCount", 0);
                // Remove item: shift all up starting from selected.
                SharedPreferences.Editor editor = sharedPrefRulesD.edit();
                for (int i = info.position + 1; i < rulesCount; i++) {
                    editor.putString(
                            "Title" + i,
                            sharedPrefRulesD.getString(
                                    "Title" + (i + 1), "error"));
                    editor.putString(
                            "Text" + i,
                            sharedPrefRulesD.getString(
                                    "Text" + (i + 1), "error"));
                    editor.putString(
                            "SSID" + i,
                            sharedPrefRulesD.getString(
                                    "SSID" + (i + 1), "error"));
                    editor.putBoolean(
                            "Enabled" + i,
                            sharedPrefRulesD.getBoolean(
                                    "Enabled" + (i + 1), false));
                }
                // Remove last item.
                editor.remove("Title" + rulesCount);
                editor.remove("Text" + rulesCount);
                editor.remove("SSID" + rulesCount);
                editor.remove("Enabled" + rulesCount);
                editor.putInt("RulesCount", rulesCount - 1);
                editor.commit();
                refreshList(true);
                return true;
            case R.id.action_edit:
                // Edit Text selected.
                final SharedPreferences sharedPrefRulesE = getSharedPreferences(
                        "rules", MODE_PRIVATE);
                // Show dialog with selected item's reminder text to edit.
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                final EditText input = new EditText(this);
                input.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS
                        | InputType.TYPE_CLASS_TEXT
                        | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT);
                input.setSingleLine(true);
                input.setHint(R.string.hint_title);
                input.setText(sharedPrefRulesE.getString(
                        "Title" + (info.position + 1), "error"));
                alert.setView(input);
                alert.setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                // OK pressed: store new value and refresh list.
                                String value = input.getText().toString();
                                if (!value.equals("")) {
                                    SharedPreferences.Editor editor = sharedPrefRulesE
                                            .edit();
                                    editor.putString(
                                            "Title"
                                                    + (info.position + 1),
                                            value);
                                    editor.commit();
                                    refreshList(true);
                                }
                            }
                        });
                alert.setNegativeButton(android.R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                // Cancel pressed.
                            }
                        });
                alert.show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public void refreshList(boolean redraw) {
        dataModels.clear();
        Bundle bundle = new Bundle();
        SharedPreferences sharedPreferences = getSharedPreferences("rules",
                MODE_PRIVATE);
        int rulesCount = sharedPreferences.getInt("RulesCount", 0);
        int activeCount = 0;
        for (int i = 1; i <= rulesCount; i++) {
            if (redraw) {
                String title = sharedPreferences.getString(
                        "Title" + i, "error");
                String ssid = sharedPreferences.getString("SSID" + i,
                        "");
                String cell = sharedPreferences.getString("Cell" + i,
                        "");
                if (cell.equals("")) {
                    title = title + " (" + ssid + ")";
                } else {
                    if (ssid.equals("")) {
                        title = title + " (" + cell + ")";
                    } else {
                        title = title + " (" + ssid + " & " + cell + ")";
                    }
                }
                Boolean enabled = sharedPreferences.getBoolean("Enabled" + i,
                        false);
                if (enabled) {
                    activeCount++;
                    // Add SSIDs of active reminders to bundle.
                    bundle.putString("SSID" + activeCount, sharedPreferences.getString("SSID" + i, "error"));
                }
                Log.d("refreshList", "Adding " + title + ", " + enabled);
                dataModels.add(new RemindersListDataModel(title, enabled));
            }
        }
        adapter.notifyDataSetChanged();
        // Add active reminders count to bundle.
        bundle.putInt("ActiveRulesCount", activeCount);
        // Enable alarm if there are active rules.
        Log.d("refreshList", "Active rules: " + activeCount);
        if (activeCount > 0) {
            startAlarm();
        } else {
            stopAlarm();
        }
    }


    public void startAlarm() {
        // Schedule job.
        Log.d("startAlarm", "Pending jobs: " + mJobScheduler.getAllPendingJobs().toString());
        boolean pollingJobExists = false;
        for (JobInfo jobInfo : mJobScheduler.getAllPendingJobs()) {
            if (jobInfo.getId() == WIFI_POLLING_JOB) {
                pollingJobExists = true;
            }
        }
        if (!pollingJobExists) {
            Log.d("startAlarm", "No scheduled polling jobs, scheduling new.");
            JobInfo.Builder builder = new JobInfo.Builder(WIFI_POLLING_JOB,
                    new ComponentName(getPackageName(), ConnectJobSchedulerService.class.getName()));
            // Execute job periodically.
            builder.setPeriodic(INTERVAL);
            // Persist job after device reboot.
            builder.setPersisted(true);
            //builder.setMinimumLatency(5000);

            if (mJobScheduler.schedule(builder.build()) <= 0) {
                Log.e("startAlarm", "Job scheduling failed.");
            } else {
                Log.d("startAlarm", "Job scheduling successful.");
            }
        } else {
            Log.d("startAlarm", "Polling job already scheduled.");
        }
    }

    public void stopAlarm() {
        mJobScheduler.cancel(WIFI_POLLING_JOB);
        Log.d("stopAlarm", "Stopping polling job.");
    }

}
